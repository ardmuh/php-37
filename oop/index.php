<?php
    Require_once('Animal.php');
    require ('Ape.php');
    require ('Frog.php');

    echo "<h2>Release 1 </h2>";

    $sheep = new Animal("shaun");

    echo "Name : " . $sheep->name . "<br>"; // "shaun"
    echo "Legs : " . $sheep->legs . "<br>"; // 4
    echo "Cold blooded : " . $sheep->cold_blooded . "<br>"; // "no"
        
        
    echo "<h2>Release 2 </h2>";
    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name . "<br>"; // "shaun"
    echo "Legs : " . $kodok->legs . "<br>"; // 4
    echo "Cold blooded : " . $kodok->cold_blooded . "<br>"; // "no"
    echo $kodok->jump() . "<br><br>"; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "Name : " . $sungokong->name . "<br>"; // "shaun"
    echo "Legs : " . $sungokong->legs . "<br>"; // 4
    echo "Cold blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
    echo  $sungokong->yell() // "Auooo"

    
?>